\documentclass[]{book}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[unicode=true]{hyperref}
\hypersetup{
            pdftitle={Resources for graduate students in the Hurford Lab},
            pdfauthor={Amy Hurford},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{longtable,booktabs}
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{5}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi
\usepackage{etoolbox}
\makeatletter
\providecommand{\subtitle}[1]{% add subtitle to \maketitle
  \apptocmd{\@title}{\par {\large #1 \par}}{}{}
}
\makeatother
% https://github.com/rstudio/rmarkdown/issues/337
\let\rmarkdownfootnote\footnote%
\def\footnote{\protect\rmarkdownfootnote}

% https://github.com/rstudio/rmarkdown/pull/252
\usepackage{titling}
\setlength{\droptitle}{-2em}

\pretitle{\vspace{\droptitle}\centering\huge}
\posttitle{\par}

\preauthor{\centering\large\emph}
\postauthor{\par}

\predate{\centering\large\emph}
\postdate{\par}

\title{Resources for graduate students in the Hurford Lab}
\author{Amy Hurford}
\date{2020-01-27}

\begin{document}
\maketitle

{
\setcounter{tocdepth}{1}
\tableofcontents
}
\chapter{Guiding principles}\label{guiding-principles}

\begin{center}\includegraphics[width=0.65\linewidth]{./logo} \end{center}

\begin{quote}
Do meaningful work, that \emph{you} are proud of, and that \emph{you}
are certain has value. Good work will eventually get rewarded, but the
path to those rewards can sometimes be long. Don't judge yourself based
on the achievements of your peers. Know \emph{your} strengths and
appreciate them.
\end{quote}

\hypertarget{supervision}{\chapter{Supervision}\label{supervision}}

At Memorial University, the expectations of supervisor and student are
outlined \href{./responsibilities.pdf}{here}.

\section{My supervisory style}\label{my-supervisory-style}

My primary priority is to help you graduate your degree. My secondary
priority is for the research contained in your thesis to be of as high
quality as possible, to maximize your chances of success on the job
market. In addition, the topic and research contained in the thesis
needs to be within the scope of my research group, as discussed during
our initial meetings. I also expect that you respect ethical standards
of good science.

The principle way that I supervise is by providing feedback on items you
have completed. This may be a research proposal, or a written summary of
your work during the past month, or a draft of a paper. Initially, I
would like to have contact with each new student once every 1-2 weeks
(depending on your workload), but as our working relationship
establishes, we can meet when there are completed items to discuss.

With regards to our working relationship you might consider the
following:

\begin{itemize}
\item
  It can be difficult to tell your supervisor, or other graduate
  students that you don't know how to do something. Will they think you
  are not smart and don't belong in graduate school? Isn't mathematical
  biology supposed to be what \emph{you're} good at; how can you ask a
  student from another lab about math biology? You can make a lot of
  progress on your thesis by asking other graduate students for help
  with your coding, talking through the interpretation of a result, the
  logic behind a hypothesis, or getting feedback on your writing. The
  most sucessful graduate students are comfortable getting help from
  other graduate students.
\item
  Developing as an independent researcher is a goal of your studies.
  There is some element of being stuck that is a normal part of
  research, and overcoming the problem yourself may be the best approach
  for your professional development. However, this isn't always the
  case, and sometimes you should seek help rather than trying to keep
  working alone.
\item
  You might want to schedule a meeting with me. If things are not
  working out as I said they would, then we need to figure out what is
  wrong. If you've just been stuck on that coding problem for too long,
  let me know to look at your code.
\item
  If you do not agree with something substantial that we discussed in a
  previous meeting, we need to have a another meeting to discuss the new
  best approach.
\end{itemize}

There are some aspects of graduate student supervision that you may not
be aware of: I write grants to secure funding that goes toward your
graduate student salary; I write reports on how these funds were used; I
send emails to organize supervisory committees; and I serve on the
supervisory and examination committees of other graduate students. I
spent a substantial amount of time on activities that are indirectly
related to supporting your studies. I want you to succeed. I will write
an honest letter of recommendation. If you are making excellent
progress, and solving a lot of problems yourself, I notice this, and I
will write about it in a reference letter.

\section{Mentoring plan}\label{mentoring-plan}

When you enter and after each year subsequently we will meet to discuss
your career plans.

\chapter{Your thesis}\label{your-thesis}

\section{Hypotheses and models}\label{hypotheses-and-models}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Design a chapter that is do-able. In my experience, a plan that is
  `just a bit more' than the current state-of-the art, will turn into
  something bigger, and might even turn out to be amazing. A good thesis
  is a done thesis. Make the hard decisions early: it is less glamorous
  to design a thesis that is feasible, but it will get you graduated and
  get you a job.
\item
  Getting a chapter finished is a great accomplishment.
\item
  If you plan for a career in academia, it can be helpful to work
  collaboratively. Know the guidelines for authorship. If you would like
  to involve a co-author, you need to discuss this with Amy.
\item
  Consider Zotero for managing your references. Use the browser plug-in
  and the add-on for MS Word.
\item
  Read the literature to formulate your hypotheses. Every thesis is
  framed within a broader context, for example,
\end{enumerate}

\begin{itemize}
\tightlist
\item
  population dynamics in seasonal environments;
\item
  population dynamics in stochastic environments;
\item
  population dynamics given an earlier age of first reproduction;
\item
  the evolution of virulence, etc.
\end{itemize}

There is lots of existing work on these general topics. Read papers from
\emph{Am Nat}, \emph{TREE}, and \emph{Ecology Letters}; journals that
value generality, until you can describe the state-of-the art facts
about a \emph{general topic}, and explain their relevance for your study
question. For example:

\begin{quote}
Threshold growth quantities, such as R0, are equivalent in seasonal and
non-seasonal environments when specific conditions, regarding
reproduction and stage-to-stage transitions, are met (Mitchell and
Kribs, 2017). While such conditions are met for sea lice, transient
dynamics are key to understanding best management practices for sea
lice, and so we aim to extend the current theory in this area.
\end{quote}

\begin{itemize}
\tightlist
\item
  The first sentence describes a specific piece of knowledge about
  seasonal population dynamics. We show scholarship and build trust with
  our reviewers by demonstrating our knowledge of Mitchell and Kribs.
  The second sentence relates this general knowledge to our specific
  application.
\end{itemize}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\setcounter{enumi}{5}
\tightlist
\item
  It is good to read about the biology of your study species, but you
  need to spend at least as much time also reading about general
  ecological/evolutionary principles.
\end{enumerate}

\section{Coding}\label{coding}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  If you are stuck on coding, perhaps the problem isn't your coding
  skills, but that don't know what you want to code. It may help to
  write out the steps of your code as pseudo-code.
\item
  Most graduate students in my lab are good coders. Talk it through with
  a labmate. Just saying it out loud can help you realize what you are
  trying to do isn't well defined.
\item
  Copy and paste the example from the help file. Then, step up the
  complexity of the problem, checking to see that it still runs after a
  small round of changes. Continue until you have modified the example
  code enough to solve your problem.
\item
  Find out the line your error is being generated from. Even in MATLAB,
  where the line of the error is reported, the actual error may have
  occurred before. You should be able to query the values assigned to
  quantities after each line is executed to identify the true line of
  the error.
\item
  I take a hypothesis testing, deductive reasoning, approach to
  debugging. For example, if a function has two arguments, then the
  error must be in the format of one or other argument, or with the
  function itself: run tests to narrow the possiblities.
\item
  Run unit tests: set the mean dispersal distance to zero; do you
  recover the expected non-spatial population dynamics? Set mortality to
  zero; does the population grow? Set the birth rate to zero; does the
  population die out?
\item
  Step through your code and query the value of your variables to make
  sure they are what you think they are.
\item
  I don't want you to be stuck on your code for long. I will help you,
  but it may take a week or so for me to find a gap in my schedule. I
  prefer to code-share with Gitlab or similar version controlled
  softwares.
\item
  If you are stuck on your code, the problem may be that you don't have
  a good strategy for debugging your code. One way to learn this can be
  to sit with someone while they debug your code and have them explain
  what they are thinking as they perform different tests.
\item
  I expect you to publish your code alongside your manuscripts and
  thesis. Archive your code on Figshare, Github, Gitlab, or similar.
\item
  The softwares I know(-ish) are MATLAB and R. MATLAB is still popular
  with mathematical biologists. R is very popular with biologists.
  MATLAB's strength is it's simplicity and the efficiency of some of the
  built-in algorithms. R's user community is now so vast that there are
  many good algorithms available. R is free, which isn't a factor during
  your graduate studies, but may become a factor at your next place of
  employment. Although I don't code in Python or C, you could complete
  your thesis in these languages if you are comfortable with them.
\end{enumerate}

\begin{itemize}
\tightlist
\item
  The lab has a high performance computer called theo (see
  \href{https://amyhurford.weebly.com/theocsmunca.html}{here} for
  instructions). If your simulations take a long time use theo or
  consider ACENET.
\end{itemize}

\section{Writing}\label{writing}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  The more familar you are with the published literature the better your
  writing will be, and the better you will be able to formulate
  hypotheses and explain your ideas.
\item
  Writing involves three steps: reading, thinking, and writing. After
  you read, you need to think about how this new knowledge ties in with
  your existing knowledge, and with the results you are rewriting about.
  If you don't know what to write, or you don't like what you've
  written, you may have skipped the thinking step.
\item
  Your paper has one main result. You did a lot of work to get to your
  results, but after doing all the work you need to organize it by
  prioritizing and de-priotizing certain elements.
\item
  The papers you cite need to fit your story. You probably don't want to
  write:
\end{enumerate}

\begin{quote}
Hurford et al. finds that wolves recolonize slower than expected.
\end{quote}

A better sentence is:

\begin{quote}
Efforts to parameterize and validate reaction-difussion and related
spread rate models have generally found that empirical spread rates are
slower than the theoretical predictions (Hurford et al.).
\end{quote}

This second sentence takes control of the narrative: to tell the story
of my results, one important topic I need to discuss is
`parameterization and validation'. I will arrange my Introduction so
that I discuss the \emph{topics} important to my results. The first
example sentence typically appears in an Introduction that is unsure of
the general topics that relate to the results (i.e., parameterization
and validation). An Introduction written this way lists papers and their
main results, and the focus is on citing the papers rather than building
a narrative around your results.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Please write in active voice and present tense. For a Methods section
  it may be okay to write in past tense, but try to keep present tense
  as much as possible.
\item
  Please see
  \href{https://cbs.umn.edu/sites/cbs.umn.edu/files/public/downloads/Annotated_Nature_abstract.pdf}{How
  to write a Nature Summary}. Writing an abstract this way asks you to
  think about the broader relevance of your work, and this is often
  something that doesn't receive enough attention.
\item
  Please see
  \href{http://parasitology.msi.ucsb.edu/sites/parasitology.msi.ucsb.edu/files/docs/publications/Writing\%20a\%20Scientific\%20Paper.doc}{Scientific
  paper outline by Kevin Lafferty}. This outline asks the writer to
  provide paragraphs that show you have summarized the relevant
  literature on your topic. One component of this is the study system,
  but this comes after summarizing the literature on the `big picture'
  question.
\item
  Start the outline of your paper by deciding what figures you will
  include. The writing needs to be concise, and planning this way will
  help keep a streamlined focus.
\item
  I recommend you learn Latex and write your thesis in it. Latex is
  stable, and MS Word usually crashes, especially when using equation
  editor in combination with copy and paste.
\item
  Get feedback from me on your writing early and often. This helps you
  to understand my expectations for your written work. If you are hoping
  to quickly finish writing your thesis, the timeline is more likely to
  proceed as you expect if you are familiar with my expectations.
\item
  Get your peers to read your writing.
\item
  \href{http://www.eeb.cornell.edu/Ellner/}{How to write a theoretical
  paper that people will cite} has good advice
\item
  Write concisely, and write the paper so that readers will find reading
  it interesting. Focus on explaining concepts and context; technical
  details are important, but they may belong in an Appendix.
\item
  Don't write things that aren't true or you aren't going to back up,
  for example, `Climate change is the biggest problem of our
  generation'; if this is an ecology paper, you won't back up that
  sentence, that sentence belongs in an sociology paper.
\end{enumerate}

\chapter{Being a graduate student}\label{being-a-graduate-student}

In graduate school, academia, and science, failure and rejection are to
be expected. It isn't fun to fail: your hopes for the future might be
dashed. I am in a position of priveledge, and so recounting my own
failures may not be relatable. For this reason, I find the following
\href{http://www.mun.ca/sgs/studentblog/a-scientist-takes-lessons-from-science/}{blog}
by Christina Prokopenko useful since it is recounted during a time of
struggle rather than retrospectively after overcoming the struggle.

Research is hard. It is research because the answers are unknown, and
not knowing the answers make it hard. For me, this makes research very
enjoyable, but sometimes I might have to work pretty hard to make some
progress. For me, the challenge is very fun.

If you believe we should do more activities as a lab, please consider
taking the initiative and organizing them.

\section{Being a gradute student at
MUN}\label{being-a-gradute-student-at-mun}

The School of Graduate studies has resoures recommended to prepare you
for graduate school \href{https://www.mun.ca/edge/Prepare/}{here}

You can get your keys to the lab by asking Amy to send Cherie an email
stating the keys you require and also completing
\href{https://www.mun.ca/biology/forms/keys-student.php}{this} form.

\subsection{Opportunities for professional
growth}\label{opportunities-for-professional-growth}

You are encouraged to attend the weekly biology seminar, the bi-weekly
Eco-Evo discussions, or other discussion groups or seminar series at
MUN. Take advantage of the community for a value-added graduate school
experience.

\subsection{Funding}\label{funding}

The sources of your funding are outlined on the Program of Study form
that you received with your acceptance letter. If your support lists
teaching assistantships (TAs), even before you start at MUN, you will
need to apply for TAships. The SGS baseline funding is guaranteed for 2
years (MSc) or 4 years (PhD).

\subsection{Student visas}\label{student-visas}

Make sure you apply for and keep your student visas current. Please
contact the International Studies Office if you need help. I am not able
to provide advice on issues related to visas.

\section{Work-life balance}\label{work-life-balance}

Traditionally, there has been a celebration of over-work in graduate
studies, but the concept of work-life balance and self care have
recently become known to be important. I leave it up to you when you
work, and I encourage you to seek balance and community during your
studies.

The purpose of the committee meetings is to outline the volume of work
and the timeframes for completion. For MSc and PhD degrees, funding is
guaranteed for 2 and 4 years respectively. In addition, please note that
you will need to submit your completed thesis several months in advance
(please click on the links under `2. Preparing for submission'
\href{https://www.mun.ca/sgs/go/guid_policies/theses.php}{here}).

All students are required to take BIOL 7000 and MSc students in biology
are required to take two courses. One class should take 12 hrs/wk. In
addition, two TAships is 120 hrs per semester (\textasciitilde{}10
hrs/wk). My hope is that providing you with these numbers will help you
plan your timeline to graduation to help you plan the work side of your
work-life balance. I encourage you to keep track of the hours you are
spending on your TA assignments and to ask, before you begin an
assignment, how long your instructor expects the work to take to
complete. Be efficient in completing your TA work.

\section{Mental health}\label{mental-health}

Several articles draw attention to a mental health crisis amongst
graduate students, for example
\href{https://blogs.scientificamerican.com/observations/the-emotional-toll-of-graduate-school/}{here}.
This article states \emph{`the issues surrounding graduate student
mental health are much easier to describe than to solve'}. Amongst the
recommendations from this article:

\begin{itemize}
\item
  \emph{`universities could require multiple advisors within a student's
  field to evaluate degree timelines'}. This is the role of the
  supervisory committee for graduate students at MUN. The supervisory
  committee does not examine, your committee's role is to support you to
  thesis submission.
\item
  \emph{`Departments could also streamline their graduation criteria to
  reduce disparities in student workload amongst different research
  groups and to increase transparency of degree requirements'}. If you
  have questions regarding how much work is sufficient for a thesis,
  these should be brought up with the supervisory committee.
\end{itemize}

The supervisor-graduate student relationship can be a source of mental
health challenges. As such, I have described my supervisory style
\protect\hyperlink{supervision}{here}.

If you are experiencing a mental health issue:

\begin{itemize}
\tightlist
\item
  If you would like to tell me, please do.
\item
  At MUN, you may seek
  \href{https://twitter.com/sgsdean/status/1176493016409923585/photo/1}{counselling}
  by emailing
  \href{mailto:gradcounselling@mun.ca}{\nolinkurl{gradcounselling@mun.ca}}.
\item
  You may wish to take a break from your studies.
\end{itemize}

\section{Diversity, equity, and
inclusion}\label{diversity-equity-and-inclusion}

The diversity within the lab strengthens our science and the science
community. I encourage all lab members to consider their implicit biases
and the structural challenges faced by underrepresented groups
(specifically women, parents, people of colour, and members of the
LGBTQ+ community) in academia and science.

Here are some ways that I consider equity, diversity, and inclusion
(EDI):

\begin{itemize}
\item
  I advertise openings in my research group internationally.
\item
  If I am an organizing an event or suggesting a reviewer, I consider
  the EDI of the group. The
  \href{https://diversifyeeb.com/}{DiversifyEEB} database is a helpful
  resource.
\item
  I consider merit relative to opportunity, and I am aware of the
  \href{https://en.wikipedia.org/wiki/Matthew_effect}{Matthew effect}
\item
  I schedule work activities within working hours.
\item
  I may attend SWEET at the CSEE meeting.
\item
  If two candidates are close, I consider if the difference between the
  candidates is bigger than my implicit biases (which would be towards
  researchers who are more like me).
\item
  The travel support that I provide to graduate students via my grants
  includes costs associated with visa applications so as not to
  disadvantage students who are citizens of specific countries.
\item
  I support parents who wish to bring their children to conferences.
\item
  I always consider that `let's go work at the pub' could exclude
  pregnant women, individuals of particular religions, or individuals
  who have had negative alcohol-related experiences.
\item
  I have taken online tests to assess my implicit bias.
\item
  I am aware that certain groups of adjectives are more likely to be
  used to describe women in reference letters.
\item
  I respect the terms of my leave, priotizing looking after my children
  during parental leave. I believe this fosters equality, so as to not
  disadvantage parents who have little local support, or difficult birth
  or post-partum circumstances.
\item
  I consider that people that belong to minority groups are likely to be
  judged more negatively during peer review and promotion.
\item
  I value everyone's efforts to improve EDI in science, even if these
  efforts take a different form than my own.
\item
  MUN has the
  \href{https://www.mun.ca/student/about/mission-vision-values.php}{Blundon
  Centre} to empower students with disabilities. I support the Blundon
  Center's initiatives.
\item
  When I teach, I emphasize the concepts rather than the individuals who
  first established the results. I believe this helps foster inclusion.
\end{itemize}

\chapter{Professional development}\label{professional-development}

\section{Talks and conferences}\label{talks-and-conferences}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  Your goal is to do good science. While graphic design and scientific
  communication are areas that you can read a lot on, unless your
  interests are specifically in these areas, remember to put the science
  first. Having said that, \href{http://basiliskos.com/downloads}{this}
  is quite good.
\item
  The problem with explaining your work clearly is that the audience
  will understand what you've done and be able to ask you challenging
  questions. Don't fear the questions: you have to explain yourself
  clearly and constructive comments will help your work in the long-run.
\item
  Organize a lab meeting to practice your talk.
\item
  Don't make jokes `everyone hates math, so I skipped the equations'.
  Individuals in the audience may have been unfairly told that `math is
  not for them'; the `everyone hates math' joke makes unfair assumptions
  about your audience's abilities and interests. Similarly, don't show a
  set of complicated equations hoping to impress everyone. Think about
  what you need to communicate for this talk, to this audience.
\item
  Respect the time limit. 1 slide per minute and 1 idea per slide are
  general rules. For a 15 minute talk you don't have to time to ad lib
  or overexplain: be organized.
\item
  When answering questions, allow yourself to pause, and think of what
  your answer will be, before talking. I can be animated and talk very
  fast. For communicating science, I activately tell myself to slow
  down, think, and be precise. It is fine to answer that you `don't
  know', especially if you explain why you don't know. Your audience
  will appreciate a carefully reasoned answer.
\item
  You may wish to use the
  \href{https://gitlab.com/ahurford/resources-for-grad-students/tree/master/lab-logo}{lab
  logo}.
\item
  Book your flights at least 6 weeks in advance. Select reasonably
  priced accommodation. Be aware of taxes and the currency of the price.
\item
  Before you travel, submit a
  \href{https://www.mun.ca/finance/forms/Travel_Request.pdf}{travel
  request} form and have it signed by Amy.
\item
  If you would like reimbursement before you travel, submit a
  \href{https://www.mun.ca/finance/forms/Travel_Advance.pdf}{travel
  advance}
\item
  Apply for travel funding from
  \href{https://www.mun.ca/sgs/current/funding/travel.php}{SGS}, the
  biology department, TAUMAN, and the
  \href{https://www.smb.org/landahl-travel-grant-application/}{SMB
  Landahl fund}.
\item
  Keep your receipts, except for food which is reimbursed at \$50/day.
  Submit your
  \href{https://www.mun.ca/finance/travel/electronic_travel.php}{travel
  claim} promptly after you return.
\end{enumerate}

\section{Reviewing}\label{reviewing}

The \href{./Peer-Review-Booklet.pdf}{BES} guide to reviewing is
excellent.

\section{Working in industry}\label{working-in-industry}

\begin{itemize}
\item
  The following is advice from a former student. This student found
  \href{https://dynamicecology.wordpress.com/2014/10/27/non-academic-careers-for-ecologists-data-science-guest-post/}{this}
  blog post helpful, and adds `most office jobs are posted at indeed.com
  and/or LinkedIn. Search by skills in addition to job titles. Different
  companies will have different names for their analyst roles, but if
  they are looking for someone who knows R/Python/MATLAB then it is
  worth a closer look.'
\item
  You may wish to consider opportunities offered through MITACS.
\end{itemize}

\chapter{Links}\label{links}

A list of
\href{https://sites.google.com/a/d.umn.edu/professional-skills/}{professional
skills} from the College of Biological at UMN Duluth.

\href{https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1006914}{Ten
simple rules towards healtheir research labs}

A \href{https://twitter.com/thehauer/status/1021179403680862218}{twitter
thread} on what I wish I knew when I started graduate school.

Stephen Stearns wrote \emph{Some modest advice to a gradaute students}.
This
\href{https://dynamicecology.wordpress.com/2015/09/08/rereading-stearns-and-hueys-some-modest-advice-to-graduate-students/}{Dynamic
ecology} blog provides some good updated discussion on this classic.

A perspective on being a
\href{http://ecoevoevoeco.blogspot.com/2018/09/adapting-to-america.html}{Chinese
scholar in the USA}; applicable to Canada too.

\end{document}
